package main

import "iris_blog/router"

func main() {
	_ = router.GetApp().Listen(":8080")
}
