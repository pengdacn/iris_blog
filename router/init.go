package router

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/pprof"
	"iris_blog/api"
	"iris_blog/modules"
)

var (
	app = iris.Default()
)

func init() {
	iris.RegisterOnInterrupt(func() {
		_ = modules.DB.Close()
	})
	html := iris.HTML("./views", ".html")
	initTmplFunc(html)
	app.RegisterView(html)
	app.HandleDir("/static", "./static")
	app.Get("/debug/pprof/{action:path}", pprof.New())
	initFront(app)
	admin := app.Party("/admin")
	admin.Use(api.AuthLogin)
	initBack(admin)
	_api := app.Party("/api", api.AuthLogin)
	initApi(_api)

}

func GetApp() *iris.Application {
	return app
}
