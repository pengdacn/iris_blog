package router

import (
	"github.com/kataras/iris/v12/view"
	"iris_blog/views"
)

func initTmplFunc(html *view.HTMLEngine) {
	html.AddFunc("tsc", views.TimeStampConversion)
	html.AddFunc("ed", views.EnumerateDate)
	html.AddFunc("dt", views.DivisionTime)
	html.AddFunc("calc", views.Calc)
	html.AddFunc("tags", views.Tags)
	html.AddFunc("markdown", views.MarkDown)
	html.AddFunc("gafc", views.GetArticleFromCommentID)
	html.AddFunc("changelog", views.ChangeLog)
	html.AddFunc("sc", views.SiteConfig)
	html.AddFunc("tn", views.TableNumber)
	html.AddFunc("na", views.GetLimitNewArticle)
	html.AddFunc("nc", views.GetLimitNewComment)
	html.AddFunc("notice", views.GetNotice)
	html.AddFunc("category", views.GetCategory)
	html.AddFunc("cn", views.GetAOfCategoryNumber)
	html.AddFunc("comment", views.GetCommentNumber)
	html.AddFunc("gavatar", views.GetGravatar)
	html.AddFunc("pna", views.PreOrNextAriticle)
	html.AddFunc("itu", views.IdToName)
	html.AddFunc("cl", views.CategoryList)
	html.AddFunc("link", views.GetAllLink)
	html.AddFunc("menu", views.GetAllMenu)
	html.AddFunc("gpb", views.GetPropertyByID)
	html.AddFunc("str2html", views.Str2html)
}
