package router

import (
	"github.com/kataras/iris/v12"
	"iris_blog/api"
	"net/http"
)

func initFront(party iris.Party) {
	party.Get("/", api.Index)
	party.Get("/{name}", api.Page)
	party.Get("/{rule}/{name}", api.Article)
	party.Get("/search", api.Search)
	party.Get("/category/{key}", api.Category)
	party.Get("/login", api.Login)
	party.Post("/login", api.Login)
	party.Get("/logout", api.Logout)
}

func initBack(party iris.Party) {
	party.Get("/", api.AdminIndex)
	party.Get("/style", api.AdminStyle)
	party.Get("/backup", api.AdminBackup)
	art := party.Party("/article")
	{
		art.Get("/", api.AdminArticle)
		art.Get("/add", api.AdminArticleAdd)
		art.Get("/update", api.AdminArticleModify)
		art.Get("/category", api.AdminCategoryAdd)
		art.Get("/category/update", api.AdminCategoryModify)

	}
	set := party.Party("/setting")
	{
		set.Get("/website", api.AdminSetting)
		set.Get("/path", api.AdminPathRewire)

	}
	party.Get("/file", api.AdminAttachment)
	party.Get("/link", api.AdminLink)
	party.Get("/link/update", api.AdminLinkModify)
	party.Get("/page", api.AdminPage)
	party.Get("/page/add", api.AdminPageAdd)
	party.Get("/page/update", api.AdminPageModify)
	party.Get("/menu", api.AdminMenu)
	party.Get("/menu/update", api.AdminMenuModify)
	party.Get("/comment", api.AdminComment)
	party.Get("/notice", api.AdminNotice)
}

func initError(app *iris.Application) {
	app.OnErrorCode(http.StatusNotFound, func(ctx iris.Context) {
		ctx.ViewData("code", 404)
		ctx.ViewData("title", "页面被吃掉了")
		_ = ctx.View("admin/error.html")
	})
}

func initApi(party iris.Party) {
	art := party.Party("/article")
	{
		initArticleApi(art)
	}
	category := art.Party("/category")
	{
		initCategoryApi(category)
	}
	page := party.Party("/page")
	{
		initPageApi(page)
	}
	comm := party.Party("/comment")
	{
		initComment(comm)
	}
	menu := party.Party("/menu")
	{
		initMenu(menu)
	}
	link := party.Party("/link")
	{
		initLink(link)
	}
	party.Post("/site/config", api.SiteModify)
}

func initArticleApi(party iris.Party) {
	party.Post("/add", api.ArticleAdd)
	party.Get("/list", api.ArticleList)
	party.Post("/update", api.ArticleModify)
	party.Get("/delete", api.ArticleDel)
}

func initCategoryApi(party iris.Party) {
	party.Get("/list", api.CategoryList)
	party.Post("/add", api.CategoryAdd)
	party.Get("/delete", api.CategoryDel)
	party.Post("/update", api.CategoryModify)
}

func initPageApi(party iris.Party) {
	party.Get("/list", api.PageList)
	party.Get("/delete", api.PageDel)
	party.Post("/add", api.PageAdd)
	party.Post("/update", api.PageModify)
}

func initComment(party iris.Party) {
	party.Post("/add", api.CommentAdd)
	party.Get("/delete", api.CommentDel)
	party.Get("/review", api.CommentReview)
	party.Get("/adopt", api.CommentAdopt)
}

func initMenu(party iris.Party) {
	party.Get("/list", api.MenuList)
	party.Delete("/delete", api.MenuDel)
	party.Post("/add", api.MenuAdd)
	party.Post("/update", api.MenuModify)
}

func initLink(party iris.Party) {
	party.Get("/list", api.LinkList)
	party.Get("/delete", api.LinkDel)
	party.Post("/add", api.LinkAdd)
	party.Post("/update", api.LinkModify)
}
