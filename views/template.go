package views

import (
	"github.com/russross/blackfriday/v2"
	"html/template"
	"iris_blog/api"
	"iris_blog/modules"
	"iris_blog/tools"
	"net/http"
	"strconv"
	"strings"
	"time"
)

/* ---------------------------------
功能： 模板函数文件
------------------------------------*/

/*******************
时间处理相关
********************/

/* 时间转换 传入时间戳字符串 是否精准 */
func TimeStampConversion(timestamp time.Time, exact bool) string {
	if exact == false {
		return timestamp.Format("2006-01-02")
	} else {
		return timestamp.Format("2006-01-02 15:04:05")
	}
}

/* 得到单独的年月日 */
func EnumerateDate(method string) string {
	now := time.Now()
	var (
		year  = string(now.Year())
		mouth = string(now.Month())
		day   = string(now.Day())
	)
	if method == "year" {
		return year
	} else if method == "mouth" {
		return mouth
	} else if method == "day" {
		return day
	} else {
		return ""
	}
}

/*******************
运算相关
********************/
/* 分页处理 */
func Calc(x, y int64, option string) int64 {
	switch option {
	case "+":
		return x + y
	case "-":
		return x - y
	default:
		return x + y
	}
}

/*******************
数据处理输出
********************/
/* 文章页面标签显示 */
func Tags(tags string) []string {
	array := strings.Split(tags, ",")
	return array
}

/* markdown转换 */
func MarkDown(content string) string {
	output := blackfriday.Run([]byte(content), blackfriday.WithExtensions(blackfriday.NoEmptyLineBeforeBlock))
	return string(output)
}

/*******************
数据关系处理
********************/
/* 由评论ID得到文章信息 */
func GetArticleFromCommentID(id int, fields string) string {
	article := modules.GetMustArticleById(modules.DB, id)
	if fields == "title" {
		return article.Title
	}
	return strconv.Itoa(id)
}

/*******************
数据获取
********************/
/* 直接调取数据库配置表-网站基本信息 */
func SiteConfig(info string) string {
	return api.Cfg.GetMust(info)
}

/* 返回表中的数据条数 用于数据统计 */
func TableNumber(table string) int {
	count := modules.GetMustTableInPiece(modules.DB, table)
	return count
}

/* 返回分类下的文章数量 */
func GetAOfCategoryNumber(cid int) int {
	count, _ := modules.GetArticleNumberWithCid(modules.DB, cid)
	return count
}

/* 获取公告表数据 */
func GetNotice() string {
	con, _ := modules.GetLastNoticeOutStr(modules.DB)
	return con
}

/* 根据分类cid 返回对应的分类名称 */
func GetCategory(id int, method string) (value string) {
	temp, _ := modules.GetCategoryById(modules.DB, id)
	if temp == nil {
		return ""
	}
	category := *temp
	if method == "name" {
		value = category.Name
	} else {
		value = category.Key
	}
	return
}

/* 返回评论的头像地址 */
func GetGravatar(email string) string {
	result := tools.StringToMd5(email, 0)
	return "http://cn.gravatar.com/avatar/" + result + "&d=identicon"
}

/* 获取当前文章的评论数量 */
func GetCommentNumber(id int) int {
	num, _ := modules.GetCommentNumByCid(modules.DB, id)
	return num
}

/* 获得当前文章的上下篇文章 */
func PreOrNextAriticle(id int, method string) map[string]string {
	if method == "pre" {
		art, _ := modules.GetArticlePreviousOnceWithId(modules.DB, id)
		return WrapArticleContext(art)
	} else {
		art, _ := modules.GetArticleNextOnceWithId(modules.DB, id)
		return WrapArticleContext(art)
	}
}

/* 根据id返回name */
func IdToName(id int) string {
	art := modules.GetMustArticleById(modules.DB, id)
	return art.Name
}

//自定义404报错
func PageNotFound(rw http.ResponseWriter, r *http.Request) {
	t, _ := template.New("error.html").ParseFiles("templates/admin/error.html")
	data := make(map[string]interface{})
	data["code"] = "404"
	data["title"] = "页面被吃掉了！"
	_ = t.Execute(rw, data)
}

/* 切割时间为 ymd : hms */
func DivisionTime(stamp time.Time) map[string]string {
	ymd := stamp.Format("2006-01-02")
	hms := stamp.Format("15:04:05")
	getTime := make(map[string]string)
	getTime["ymd"] = ymd
	getTime["hms"] = hms
	return getTime
}

func ChangeLog() *ChangeLogData {
	chg := new(ChangeLogData)
	return chg
}

type ChangeLogData struct {
	Email   string `json:"Email"`
	Name    string `json:"Name"`
	Version []struct {
		Commit    string `json:"Commit"`
		Committer string `json:"Committer"`
		Date      string `json:"Date"`
		Tag       string `json:"Tag"`
		Title     string `json:"Title"`
		Type      string `json:"Type"`
		Level     string `json:"Level"`
		Version   string `json:"Version"`
	} `json:"Version"`
	Website string `json:"Website"`
}

////官方最新日志获取
//func ChangeLog() ChangeLogData {
//	data := ChangeLogData{}
//	//ReadFile函数会读取文件的全部内容，并将结果以[]byte类型返回
//	resp, err := http.Get("https://gitee.com/xuthus5/GoMD/raw/master/CHANGELOG.json")
//	if err != nil {
//		beego.Error("获取更新信息失败")
//		content, err := ioutil.ReadFile("CHANGELOG.json")
//		if err != nil {
//			beego.Error("获取本地更新信息失败")
//		}
//		err = json.Unmarshal(content, &data)
//	} else {
//		body, err := ioutil.ReadAll(resp.Body)
//		if err != nil {
//			beego.Error("读取更新信息失败！")
//		}
//		//读取的数据为json格式，需要进行解码
//		err = json.Unmarshal(body, &data)
//		if err != nil {
//			beego.Error("解析json数据失败")
//		}
//	}
//	defer resp.Body.Close()
//	return data
//}

func WrapArticleContext(art *modules.Article) map[string]string {
	res := make(map[string]string)
	if art == nil {
		res["isNull"] = "true"
		res["title"] = ""
		res["name"] = ""
	} else {
		res["isNull"] = "false"
		res["title"] = art.Title
		res["name"] = art.Name
	}
	return res
}

func Str2html(raw string) template.HTML {
	return template.HTML(raw)
}

// 为使用GoMD模板所写的兼容函数

func GetLimitNewArticle(num int) []modules.Article {
	arts, _ := modules.GetArticleMaxPiece(modules.DB, num)
	return arts
}

func GetLimitNewComment(num int, stat bool) []modules.Comment {
	db := modules.DB
	if stat {
		db = db.Where("status = 1")
	}
	comments, _ := modules.GetCommentLastNum(db, num)
	return comments
}

func CategoryList() []modules.Category {
	cs, _ := modules.GetCategoryAll(modules.DB)
	return cs
}

func GetAllLink() []modules.Link {
	links, _ := modules.GetLinkAll(modules.DB)
	return links
}

func GetAllMenu() []modules.Link {
	links, _ := modules.GetLinkAll(modules.DB.Where("type = 1"))
	return links
}

func GetPropertyByID(id int, property string) string {
	art := modules.GetMustArticleById(modules.DB, id)
	var rst string
	if property == "title" {
		rst = art.Title
	}
	return rst
}
