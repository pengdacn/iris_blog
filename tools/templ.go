package tools

import (
	"os"
	"path/filepath"
	"strings"
)

func ParseTemplate(dir string, match func(string) bool) map[string]string {
	temp := make(map[string]string)
	_ = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if !match(path) {
			return nil
		}
		t := strings.Index(path, "/")
		temp[path[t+1:]] = path
		return nil
	})
	return temp
}
