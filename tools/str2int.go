package tools

import (
	"github.com/pkg/errors"
	"strconv"
)

func TransformMustInt(str string) int {
	if i, err := strconv.Atoi(str); err != nil {
		panic(errors.Wrap(err, "转换数值失败"))
	} else {
		return i
	}
}
