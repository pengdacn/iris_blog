package tools

import (
	"crypto/md5"
	"encoding/hex"
)

/* 字符串转md5 */
func StringToMd5(str string, length int64) string {
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(str))
	data := hex.EncodeToString(md5Ctx.Sum(nil))
	if length == 0 {
		return data
	} else {
		return data[0:length]
	}
}
