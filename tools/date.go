package tools

import (
	"strconv"
	"time"
)

func DivisionTime(stamp string) (ymd, hms string) {
	stamp = stamp[:10]
	base, _ := strconv.ParseInt(stamp, 10, 64)
	YMDLayout := "2006-01-02"
	HMSLayout := "15:04:05"
	ymd = time.Unix(base, 0).Format(YMDLayout)
	hms = time.Unix(base, 0).Format(HMSLayout)
	return
}
