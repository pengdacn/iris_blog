package tools

import "math"

type Paging struct {
	Page      int
	PageSize  int
	Total     int
	PageCount int
}

func NewPaging(page, pageSize, total int) *Paging {
	p := new(Paging)
	_page := page
	if page < 1 {
		_page = 1
	}
	p.Page = _page
	_pageSize := pageSize
	if pageSize < 1 {
		_pageSize = 1
	}
	p.PageSize = _pageSize
	_total := total
	if total < 0 {
		_total = 0
	}
	p.Total = _total
	p.PageCount = int(math.Ceil(float64(_total) / float64(_pageSize)))
	return p
}
