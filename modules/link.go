package modules

import (
	"errors"
	"github.com/jinzhu/gorm"
)

func GetLinkAll(db *gorm.DB) ([]Link, error) {
	links := make([]Link, 0)
	err := db.Table("link").Order("id").Scan(&links).Error
	return links, err
}

func GetLinkById(db *gorm.DB, id int) (*Link, error) {
	var l Link
	err := db.Table("link").Where("id = ?", id).First(&l).Error
	return &l, err
}

func (l *Link) Insert(db *gorm.DB) error {
	return db.Table("link").Create(l).Error
}

func (l *Link) Update(db *gorm.DB) error {
	if l.ID == -1 {
		return errors.New("错误的id")
	}
	return db.Table("link").Where("id = ?", l.ID).Update(&l).Error
}

func DelLinkByID(db *gorm.DB, id int) error {
	return db.Table("link").Where("id = ?", id).Delete(&Link{}).Error
}
