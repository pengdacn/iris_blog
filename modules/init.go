package modules

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"iris_blog/system"
	"time"
)

var DB *gorm.DB

func init() {
	switch system.SysConfig.DbType {
	case "splite3":
		sqlite3Init(&DB)
	default:
		panic(errors.New("请配置使用正确的数据库"))
	}
	DB.SingularTable(true)
	if system.SysConfig.RunMode < system.Produce {
		DB.LogMode(true)
	}
	initTable := []interface{}{
		new(Config),
		new(Article),
		new(Category),
		new(Comment),
		new(Link),
		new(Notice),
	}
	for _, item := range initTable {
		if err := DB.AutoMigrate(item).Error; err != nil {
			panic(err)
		}
	}
	installed := new(Config)
	if err := DB.Table("Config").Where("option = ?", "IsInstall").First(installed).Error; err != nil {
		if err != gorm.ErrRecordNotFound {
			panic(errors.WithStack(err))
		}
		if installed.Value != "OK" {
			if err := initSysConfig(DB); err != nil {
				panic(errors.Wrap(err, "系统初始化失败"))
			}
		}
	}
}

func initSysConfig(db *gorm.DB) error {
	cfgs := []Config{
		{Option: "IsInstall", Value: "OK"},
		{Option: "WebTitle", Value: "pengda的博客"},
		{Option: "Author", Value: "panda"},
		{Option: "Password", Value: "happy2020"},
		{Option: "PageSize", Value: "10"},
		{Option: "update", Value: time.Now().Format("2006-01-02 15:04:05.999999999 -0700 MST")},
		{Option: "Theme", Value: "QuietV1"},
		{Option: "SecondaryTitle", Value: "天道酬勤"},
		{Option: "UserImageUrl", Value: "/static/common/images/user-head-image.jpeg"},
		{Option: "LogoUrl", Value: "/static/common/images/user-head-image.jpeg"},
		{Option: "UserEmail"},
		{Option: "UserGithub"},
		{Option: "UserGitee"},
		{Option: "Rewrite", Value: "1"},
		{Option: "Repath", Value: "/article/{name}"},
		{Option: "Comment", Value: "off"},
		{Option: "UserQQ"},
		{Option: "Keywords"},
		{Option: "WebUrl"},
		{Option: "Description"},
		{Option: "Status"},
	}
	for i := 0; i < len(cfgs); i++ {
		if err := cfgs[i].Insert(db); err != nil {
			return err
		}
	}
	return nil
}
