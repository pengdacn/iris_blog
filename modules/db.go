package modules

import "time"

type Config struct {
	ID     int
	Option string
	Value  string
}

type Article struct {
	ID       int `json:"Id"`
	Cid      int `form:"cid"`
	Count    int
	Type     int
	Name     string `form:"name"`
	Title    string `form:"title"`
	Author   string `form:"author"`
	Image    string
	Tags     string `form:"tags"`
	Renew    time.Time
	Content  string `form:"content"`
	Summary  string `form:"summary"`
	Status   string
	Password string
	Link     string
}

type Comment struct {
	ID      int `json:"Id"`
	Aid     int `json:"aid"`
	Status  int `json:"status"`
	Reply   int
	Content string    `json:"content" form:"content"`
	Data    time.Time `json:"data" form:"data"`
	Email   string    `json:"email" form:"email"`
	Name    string    `json:"name" form:"name"`
	Link    string    `json:"link" form:"link"`
}

type Category struct {
	ID          int    `json:"Id"`
	Parent      int    `json:"parent"`
	Name        string `form:"name"`
	Key         string `form:"key"`
	Method      string
	Description string `form:"description"`
}

type Notice struct {
	ID      int `json:"Id"`
	Aid     int
	Status  int
	Reply   int
	Content string
	Date    time.Time
	Email   string
	Name    string
	Link    string
}

type Link struct {
	ID          int `json:"Id"`
	Url         string
	Name        string
	Image       string
	Target      string
	Description string
	Visible     string
	Type        int
	Sort        int
}

type CategoryData struct {
	IsNil bool
	Msg   string
	Info  []Category
	List  []Article
}
