package modules

import (
	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
	"iris_blog/system"
)

func sqlite3Init(db **gorm.DB) {
	var err error
	*db, err = gorm.Open("sqlite3", system.SysSqlite3.Path)
	if err != nil {
		panic(errors.Wrap(err, "数据库打开失败"))
	}
}
