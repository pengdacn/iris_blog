package modules

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

func GetCategoryById(db *gorm.DB, id int) (c *Category, err error) {
	c = new(Category)
	err = db.Table("category").First(c, "id = ?", id).Error
	return
}

func GetMustCategoryById(db *gorm.DB, id int) (c *Category) {
	c, err := GetCategoryById(db, id)
	if gorm.IsRecordNotFoundError(err) {
		return new(Category)
	}
	if err != nil {
		panic(errors.WithStack(err))
	}
	return
}

func GetCategoryAll(db *gorm.DB) ([]Category, error) {
	cs := make([]Category, 0)
	err := db.Table("category").Scan(&cs).Error
	return cs, err
}

func GetCategoryByKey(db *gorm.DB, key string) ([]Category, error) {
	cs := make([]Category, 0)
	err := db.Table("category").Where("key = ?", key).Scan(&cs).Error
	return cs, err
}

func (c *Category) InsertIdentified(db *gorm.DB) error {
	var (
		nameCount int
		keyCount  int
	)
	if err := db.Table("category").Where("name = ?", c.Name).Count(&nameCount).Error; err != nil {
		return err
	}
	if err := db.Table("category").Where("key = ?", c.Key).Count(&keyCount).Error; err != nil {
		return err
	}
	if keyCount != 0 || nameCount != 0 {
		return errors.New("key或者name重复")
	}
	return db.Table("category").Create(c).Error
}

func DelCategoryById(db *gorm.DB, id int) error {
	return db.Table("category").Delete(&Category{}, "id = ?", id).Error
}

func (c *Category) UpdateByID(db *gorm.DB) error {
	if c.ID == 0 {
		return errors.New("id error")
	}
	return db.Table("category").Where("id = ?", c.ID).Updates(c).Error
}
