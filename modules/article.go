package modules

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"time"
)

func GetLimitArticle(db *gorm.DB, start, end int) ([]Article, int, error) {
	arts := make([]Article, 0)
	var count int
	if err := db.Table("article").Count(&count).Error; err != nil {
		return nil, 0, err
	}
	if err := db.Table("article").Scan(&arts).Offset(start).Limit(end - start).Error; err != nil {
		return nil, 0, err
	}
	return arts, count, nil
}

// 该方法不会验证page，pageSize的合法性，由调用方负责保证
func GetPageArticle(db *gorm.DB, page, pageSize int) ([]Article, int, error) {
	start := (page - 1) * pageSize
	end := page * pageSize
	return GetLimitArticle(db, start, end)
}

func GetArticleMaxPiece(db *gorm.DB, num int) ([]Article, error) {
	arts, _, err := GetLimitArticle(db, 0, num)
	return arts, err
}

func GetArticleById(db *gorm.DB, id int) (*Article, error) {
	art := new(Article)
	if err := db.Where("id = ?", id).First(art).Error; err != nil {
		return nil, err
	}
	return art, nil
}

func GetArticleByCid(db *gorm.DB, cid int) ([]Article, error) {
	art := make([]Article, 0)
	if err := db.Where("cid = ?", cid).Scan(art).Error; err != nil {
		return nil, err
	}
	return art, nil
}

func GetArticleByName(db *gorm.DB, name string) (rst []Article, err error) {
	if err = db.Table("article").Where("name = ?", name).Scan(&rst).Error; err != nil {
		return nil, err
	}
	return
}

func GetMustArticleByName(db *gorm.DB, name string) (rst []Article) {
	rst, err := GetArticleByName(db, name)
	if err != nil {
		panic(errors.WithStack(err))
	}
	return
}

func GetMustArticleById(db *gorm.DB, id int) *Article {
	art, err := GetArticleById(db, id)
	if err != nil {
		panic(errors.WithStack(err))
	}
	return art
}

func GetArticleNumberWithCid(db *gorm.DB, cid int) (int, error) {
	var count int
	if err := db.Table("article").Where("cid = ?", cid).Count(&count).Error; err != nil {
		return 0, err
	}
	return count, nil
}

func GetArticlePreviousOnceWithId(db *gorm.DB, id int) (*Article, error) {
	sql := db.Table("article").Where("id = ?",
		db.Table("article").Select("id").Order("id desc").Where("id < ?", id).Limit(1).SubQuery(),
	)
	rst := new(Article)
	if err := sql.First(rst).Error; err != nil {
		return nil, errors.WithStack(err)
	}
	return rst, nil
}

func GetArticleNextOnceWithId(db *gorm.DB, id int) (*Article, error) {
	sql := db.Table("article").Where("id = ?",
		db.Table("article").Select("id").Order("id").Where("id > ?", id).Limit(1).SubQuery(),
	)
	rst := new(Article)
	if err := sql.First(rst).Error; err != nil {
		return nil, errors.WithStack(err)
	}
	return rst, nil
}

func SearchArticle(db *gorm.DB, keywords string) (arts []Article, err error) {
	sql := db.Table("article").Where("type = 0").Where("and `title`||`content`||`tags` like", fmt.Sprintf("%%%s%%", keywords))
	err = sql.First(&arts).Error
	return
}

func MustSearchArticle(db *gorm.DB, keywords string) (arts []Article) {
	arts, err := SearchArticle(db, keywords)
	if err != nil {
		panic(errors.WithStack(err))
	}
	return
}

func (a *Article) Insert(db *gorm.DB) error {
	a.Renew = time.Now()
	return db.Table("article").Create(a).Error
}

func (a *Article) Update(db *gorm.DB) error {
	a.Renew = time.Now()
	if a.ID != 0 {
		return db.Table("article").Where("id = ?", a.ID).Updates(a).Error
	}
	return errors.New("主键为空")
}

func GetArticleAll(db *gorm.DB) (arts []Article, err error) {
	err = db.Table("article").Scan(&arts).Error
	return
}

func DelArticleById(db *gorm.DB, id int) error {
	return db.Table("article").Where("id = ?", id).Delete(&Article{}).Error
}
