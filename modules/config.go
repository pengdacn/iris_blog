package modules

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

func GetConfigList(db *gorm.DB) ([]Config, error) {
	cfg := make([]Config, 0)
	if err := db.Table("config").Scan(&cfg).Error; err != nil {
		return nil, errors.Wrap(err, "获取config list 失败")
	}
	return cfg, nil
}

func GetConfigListWrapMap(db *gorm.DB) (map[string]string, error) {
	var (
		c   []Config
		err error
	)
	if c, err = GetConfigList(db); err != nil {
		return nil, err
	}
	cfg := make(map[string]string)
	for i := 0; i < len(c); i++ {
		key, val := c[i].Option, c[i].Value
		cfg[key] = val
	}
	return cfg, nil
}

func GetOneConfigByOption(db *gorm.DB, option string) (*Config, error) {
	cfg := new(Config)
	if err := db.Table("config").Where("option = ?", option).First(cfg).Error; err != nil {
		return nil, errors.WithStack(err)
	}
	return cfg, nil
}

// 插入时有重复的数据，回替换原有的项
func (c *Config) Insert(db *gorm.DB) error {
	cdb := db.Table("config")
	var i int
	if err := cdb.Where("option = ?", c.Option).Count(&i).Error; err != nil {
		return err
	}
	if i > 0 {
		if err := cdb.Where("option = ?", c.Option).Updates(c).Error; err != nil {
			return err
		}
	}
	if err := cdb.Create(c).Error; err != nil {
		return err
	}
	return nil
}

func (c *Config) Update(db *gorm.DB) error {
	return db.Table("config").Where("option = ?", c.Option).Update("value", c.Value).Error
}
