package modules

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"time"
)

func GetCommentsByCid(db *gorm.DB, cid int) (comments []Comment, err error) {
	err = db.Table("comment").Where("cid = ?", cid).Scan(&comments).Error
	return
}

func GetMustCommentsByCid(db *gorm.DB, cid int) (comments []Comment) {
	comments, err := GetCommentsByCid(db, cid)
	if err != nil {
		panic(errors.WithStack(err))
	}
	return
}

func GetCommentsByAid(db *gorm.DB, aid int) (comments []Comment, err error) {
	err = db.Table("comment").Where("Aid = ?", aid).Scan(&comments).Error
	return
}

func GetMustCommentsByAid(db *gorm.DB, aid int) (comments []Comment) {
	comments, err := GetCommentsByAid(db, aid)
	if gorm.IsRecordNotFoundError(err) {
		return
	}
	if err != nil {
		panic(errors.WithStack(err))
	}
	return
}

func GetCommentNumByCid(db *gorm.DB, cid int) (int, error) {
	var count int
	err := db.Table("comment").Where("cid = ?", cid).Count(&count).Error
	return count, err
}

func GetCommentLastNum(db *gorm.DB, num int) ([]Comment, error) {
	comments := make([]Comment, 0)
	err := db.Table("comment").Order("date desc").Scan(&comments).Error
	return comments, err
}

func (c *Comment) Insert(db *gorm.DB) error {
	c.Data = time.Now()
	return db.Table("comment").Create(c).Error
}

func DelCommentByID(db *gorm.DB, id int) error {
	return db.Table("comment").Where("id = ?", id).Delete(&Comment{}).Error
}
