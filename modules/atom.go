package modules

import (
	"github.com/jinzhu/gorm"
	"time"
)

// 事务原子操作

// Config更新原子操作，在每次更新时设置update字段的时间为当前时间
func InsertConfig(db *gorm.DB, c *Config) error {
	return db.Transaction(func(tx *gorm.DB) error {
		if err := c.Insert(tx); err != nil {
			return err
		}
		uptime := &Config{
			Option: "update",
			Value:  time.Now().Format("2006-01-02 15:04:05.999999999 -0700 MST"),
		}
		return uptime.Insert(tx)
	})
}
