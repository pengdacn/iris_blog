package modules

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

func GetLastNoticeOutStr(db *gorm.DB) (string, error) {
	notice := new(Notice)
	if err := db.Order("date desc").Limit(1).Scan(notice).Error; err != nil {
		return "", errors.WithStack(err)
	}
	return notice.Content, nil
}
