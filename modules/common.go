package modules

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

func GetTableInPiece(db *gorm.DB, tab string) (int, error) {
	var total int
	if err := db.Table(tab).Count(&total).Error; err != nil {
		return 0, err
	}
	return total, nil
}

func GetMustTableInPiece(db *gorm.DB, tab string) int {
	total, err := GetTableInPiece(db, tab)
	if err != nil {
		panic(errors.WithStack(err))
	}
	return total
}
