package system

import (
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

var (
	SysConfig  = new(config)
	SysSqlite3 = new(splite3)
)

func init() {
	viper.AddConfigPath("./conf")
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.SetDefault("ListenAddr", "127.0.0.1")
	viper.SetDefault("ListenPort", "8080")
	viper.SetDefault("runMode", "Unknown")
	viper.SetDefault("db.type", "sqlite3")
	err := viper.ReadInConfig()
	if err != nil {
		panic(errors.Wrap(err, "读取配置文件失败"))
	}
	initConfig(SysConfig)
	switch viper.GetString("db.type") {
	case "splite3":
		initSplite3(SysSqlite3)
	}
}
