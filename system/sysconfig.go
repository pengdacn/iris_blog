package system

import (
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// system包下主要存放pblog配置的struct,以及其方法

type config struct {
	RunMode    runMode
	ListenAddr string
	ListenPort string
	DbType     string
}

func initConfig(conf *config) {
	conf.ListenAddr = viper.GetString("ListenAddr")
	conf.ListenPort = viper.GetString("ListenPort")
	conf.RunMode = str2RunMode(viper.GetString("runMode"))
	conf.DbType = viper.GetString("db.type")
}

type runMode uint8

const (
	// 数值越小,debug越详细
	Developer runMode = iota
	Test
	Produce
	Unknown = Developer
)

func (r runMode) String() string {
	switch r {
	case Developer:
		return "Developer"
	case Test:
		return "Test"
	case Produce:
		return "Produce"
	default:
		return "UNKNOWN"
	}
}

func str2RunMode(str string) runMode {
	switch str {
	case "Developer":
		return Developer
	case "Test":
		return Test
	case "Produce":
		return Produce
	}
	return Unknown
}

type splite3 struct {
	Path string
}

func initSplite3(s *splite3) {
	if str := viper.GetString("splite3.path"); str != "" {
		s.Path = str
		return
	}
	panic(errors.New("没有设置splite3配置的地址"))
}
