package api

import (
	"github.com/kataras/iris/v12"
	"iris_blog/modules"
)

func AdminIndex(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewData("rule", Cfg.GetRule())
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/index.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminStyle(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/index.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminBackup(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/backup.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminArticleAdd(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewData("rule", Cfg.GetRule())
	if l, err := modules.GetCategoryAll(modules.DB); err == nil {
		ctx.ViewData("list", l)
	} else {
		ctx.Application().Logger().Println(err)
		ctx.ViewData("list", make([]modules.Category, 0))
	}
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/article/add.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminPageAdd(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewData("rule", Cfg.GetRule())
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/page/add.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminArticle(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/article/list.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminPage(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/page/list.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminNotice(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/manager/notice.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminAttachment(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/manager/attachment.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminLink(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/manager/link.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminMenu(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/manager/menu.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminMenuModify(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	id, _ := ctx.URLParamInt("id")
	link, err := modules.GetLinkById(modules.DB, id)
	if err != nil {
		ctx.Application().Logger().Println(err)
		return
	}
	ctx.ViewData("master", master)
	ctx.ViewData("link", []*modules.Link{link})
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/manager/menu-update.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminComment(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/manager/comment.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminSetting(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewData("theme", GetThemeDir())
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/setting/website.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminPathRewire(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/setting/path.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminArticleModify(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	id, _ := ctx.URLParamInt("id")
	article, err := modules.GetArticleById(modules.DB, id)
	if err != nil {
		ctx.Application().Logger().Println(err)
		return
	}
	categories, err := modules.GetCategoryAll(modules.DB)
	if err != nil {
		ctx.Application().Logger().Println(err)
		return
	}
	ctx.ViewData("article", []*modules.Article{article})
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewData("rule", Cfg.GetCfg())
	ctx.ViewData("category", categories)
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/article/update.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminPageModify(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewErr("错误的get请求"))
		ctx.StopExecution()
		return
	}
	article, err := modules.GetArticleById(modules.DB, id)
	if err != nil {
		ctx.Application().Logger().Println(err)
		return
	}
	categories, err := modules.GetCategoryAll(modules.DB)
	if err != nil {
		ctx.Application().Logger().Println(err)
		return
	}
	ctx.ViewData("article", article)
	ctx.ViewData("master", master)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewData("rule", Cfg.GetCfg())
	ctx.ViewData("category", categories)
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/article/update.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminCategoryAdd(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	categories, err := modules.GetCategoryAll(modules.DB)
	if err != nil {
		ctx.Application().Logger().Println(err)
		return
	}
	ctx.ViewData("master", master)
	ctx.ViewData("list", categories)
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/article/category.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminCategoryModify(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	id, _ := ctx.URLParamInt("id")
	category, err := modules.GetCategoryById(modules.DB, id)
	if err != nil {
		ctx.Application().Logger().Println(err)
		return
	}
	ctx.ViewData("master", master)
	ctx.ViewData("category", []*modules.Category{category})
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/article/category-update.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func AdminLinkModify(ctx iris.Context) {
	master := ctx.Values().GetString("master")
	id, _ := ctx.URLParamInt("id")
	link, err := modules.GetLinkById(modules.DB, id)
	if err != nil {
		ctx.Application().Logger().Println(err)
		return
	}
	ctx.ViewData("master", master)
	ctx.ViewData("link", []*modules.Link{link})
	ctx.ViewLayout("admin/layout.html")
	if err := ctx.View("admin/manager/link-update.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}
