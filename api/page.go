package api

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"iris_blog/modules"
)

func PageAdd(ctx iris.Context) {
	art := new(modules.Article)
	if err := ctx.ReadForm(art); err != nil {
		_, _ = ctx.JSON(NewJErr(1, err.Error()))
		ctx.StopExecution()
		return
	}
	art.Type = 1
	if err := art.Insert(modules.DB); err != nil {
		_, _ = ctx.JSON(NewJErr(1, err.Error()))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewJOk("创建成功"))
}

func PageList(ctx iris.Context) {
	order := ctx.URLParam("order")
	switch order {
	case "asc":
	case "desc":
	default:
		order = "asc"
	}
	arts, err := modules.GetArticleAll(modules.DB.Where("type = 1").Order(fmt.Sprintf("ID %s", order)))
	if err != nil {
		_, _ = ctx.JSON(NewJErr(1, "查询错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewJRstOk(arts))
}

func PageModify(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {

	}
	page := new(modules.Article)
	if ctx.ReadForm(page) != nil {
		_, _ = ctx.JSON(NewJErr(1, "form 信息错误"))
		ctx.StopExecution()
		return
	}
	page.ID = id
	if page.Update(modules.DB) != nil {
		_, _ = ctx.JSON(NewJErr(1, "数据更新错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewJOk())
}

func PageDel(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewJErr(1, "get 参数错误"))
		ctx.StopExecution()
		return
	}
	err = modules.DelArticleById(modules.DB, id)
	if err != nil {
		_, _ = ctx.JSON(NewJErr(1, "数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewJOk())
}
