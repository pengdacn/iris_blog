package api

import (
	"github.com/kataras/iris/v12"
	"iris_blog/modules"
	"iris_blog/tools"
	"strings"
)

func Index(ctx iris.Context) {
	page := ctx.URLParamIntDefault("page", 1)
	var pageSize = 10
	if s, ok := Cfg.Get("pageSize"); ok {
		pageSize = tools.TransformMustInt(s)
	} else {
		pageSize = 10
	}
	arts, total, err := modules.GetPageArticle(modules.DB, page, pageSize)
	if err != nil {
		ctx.Application().Logger().Printf("读取数据库错误 %s", err)
		return
	}
	var (
		rewrite string
		rule    string
	)
	if _rewrite := Cfg.GetMust("Rewrite"); _rewrite == "1" {
		rewrite = "true"
		rule = "article"
	} else {
		rewrite = "false"
		rule = strings.Split(_rewrite, "/")[1]
	}
	ctx.ViewLayout("QuietV1/layout.html")
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewData("rewrite", rewrite)
	ctx.ViewData("rule", rule)
	ctx.ViewData("list", arts)
	ctx.ViewData("paging", tools.NewPaging(page, pageSize, total))
	if err := ctx.View("QuietV1/index.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func Page(ctx iris.Context) {
	name := ctx.Params().GetString("name")
	var rewrite string
	if Cfg.GetMust("Rewrite") == "1" {
		names := strings.Split(name, ".")
		name = names[0]
		rewrite = "true"
	} else {
		rewrite = "false"
	}
	article, err := modules.GetArticleByName(modules.DB, name)
	if err != nil {
		ctx.Application().Logger().Printf("读取数据库错误 %s", err)
		return
	}
	comments, err := modules.GetCommentsByAid(modules.DB, article[0].ID)
	if err != nil {
		ctx.Application().Logger().Printf("读取数据库错误 %s", err)
		return
	}
	ctx.ViewLayout("QuietV1/layout.html")
	ctx.ViewData("id", article[0].ID)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewData("article", article)
	ctx.ViewData("comments", comments)
	ctx.ViewData("rewrite", rewrite)
	if err := ctx.View("QuietV1/page.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func Article(ctx iris.Context) {
	name := ctx.Params().GetString("name")
	rule := ctx.Params().GetString("rule")
	var rewirte string
	if Cfg.GetMust("Rewrite") == "1" {
		name = strings.Split(name, ".")[0]
		rewirte = "true"
	} else {
		rewirte = "false"
	}
	article := modules.GetMustArticleByName(modules.DB, name)
	label := modules.GetMustCategoryById(modules.DB, article[0].Cid)
	comments := modules.GetMustCommentsByAid(modules.DB, article[0].ID)
	ctx.ViewLayout("QuietV1/layout.html")
	ctx.ViewData("article", article)
	ctx.ViewData("config", Cfg.GetCfg())
	ctx.ViewData("id", article[0].ID)
	ctx.ViewData("rule", rule)
	ctx.ViewData("rewrite", rewirte)
	ctx.ViewData("comments", comments)
	ctx.ViewData("label", label)
	if err := ctx.View("QuietV1/article.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func Search(ctx iris.Context) {
	c := newCon(ctx)
	keywords := ctx.URLParamDefault("keywords", "")
	if keywords == "" {
		c.logPrintln("search keywords is empty")
		return
	}
	var (
		rule    string
		rewrite string
	)
	if str := Cfg.GetMust("Rewrite"); str == "1" {
		rule = strings.Split(str, "/")[1]
		rewrite = "true"
	} else {
		rule = "article"
		rewrite = "false"
	}
	arts, err := modules.SearchArticle(modules.DB, keywords)
	if err != nil {
		c.logPrintln(err)
		return
	}
	c.addViewData(iris.Map{
		"list":     arts,
		"keywords": keywords,
		"config":   Cfg.GetCfg(),
		"rule":     rule,
		"rewrite":  rewrite,
	})
	ctx.ViewLayout("QuietV1/layout.html")
	if err := ctx.View("QuietV1/search.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func Category(ctx iris.Context) {
	c := newCon(ctx)
	key := ctx.Params().GetString("key")
	var (
		rule    string
		rewrite string
	)
	if Cfg.GetMust("Rewrite") == "1" {
		rewrite = "true"
		rule = strings.Split(Cfg.GetMust("Rewrite"), "/")[1]
	} else {
		rule = "article"
		rewrite = "false"
	}
	categorys, err := modules.GetCategoryByKey(modules.DB, key)
	if err != nil {
		c.logPrintln(err)
		return
	}
	var rst modules.CategoryData
	if len(categorys) == 0 {
		rst.IsNil = true
		rst.Msg = "分类不存在!"
	} else {
		arts, err := modules.GetArticleByCid(modules.DB, categorys[0].ID)
		if err != nil {
			c.logPrintln(err)
			return
		}
		rst.Info = categorys
		rst.List = arts
	}
	c.addViewData(iris.Map{
		"data":    rst,
		"config":  Cfg.GetCfg(),
		"rule":    rule,
		"rewrite": rewrite,
	})
	ctx.ViewLayout("QuietV1/layout.html")
	if err := ctx.View("QuietV1/category.html"); err != nil {
		ctx.Application().Logger().Println(err)
	}
}
