package api

import (
	"github.com/pkg/errors"
	"iris_blog/modules"
	"strings"
	"sync"
	"time"
)

type _config struct {
	sync.RWMutex
	update time.Time
	cfg    map[string]string
}

// 从config表中读取的配置项
var Cfg _config

func init() {
	initCfg()
}

func initCfg() {
	_cfg, err := modules.GetConfigListWrapMap(modules.DB)
	if err != nil {
		panic(errors.Wrap(err, "加载config表失败"))
	}
	Cfg.load(_cfg)
}

func (c *_config) Get(key string) (string, bool) {
	var (
		val string
		ok  bool
	)
	c.RLock()
	val, ok = c.cfg[key]
	c.RUnlock()
	return val, ok
}

func (c *_config) GetMust(key string) string {
	if val, ok := c.Get(key); ok {
		return val
	}
	return ""
}

func (c *_config) GetCfg() map[string]string {
	m := make(map[string]string)
	for k, v := range c.cfg {
		m[k] = v
	}
	return m
}

func (c *_config) GetList() []modules.Config {
	c.RLock()
	defer c.RUnlock()
	cfgs := make([]modules.Config, 0, len(c.cfg))
	for k, v := range c.cfg {
		cfgs = append(cfgs, modules.Config{Option: k, Value: v})
	}
	return cfgs
}

func (c *_config) set(key, val string) {
	c.Lock()
	defer c.Unlock()
	c.cfg[key] = val
}

func (c *_config) load(cfg map[string]string) {
	c.Lock()
	defer c.Unlock()
	c.cfg = cfg
	_time, err := time.Parse("2006-01-02 15:04:05.999999999 -0700 MST", cfg["update"])
	if err != nil {
		panic(errors.Wrap(err, "加载config表是解析时间格式失败"))
	}
	c.update = _time
}

func (c *_config) GetRule() string {
	var rule string
	if s := strings.Split(c.GetMust("Repath"), "/"); len(s) > 2 {
		rule = s[1]
	} else {
		rule = "article"
	}
	return rule
}
