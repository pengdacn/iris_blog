package api

import (
	"github.com/kataras/iris/v12"
	"iris_blog/modules"
	"time"
)

func ArticleAdd(ctx iris.Context) {
	art := new(modules.Article)
	if err := ctx.ReadForm(art); err != nil {
		ctx.Application().Logger().Println(err)
		_, _ = ctx.JSON(NewErr("错误的表单数据"))
		ctx.StopExecution()
		return
	}
	if art.Name == "" {
		art.Name = dict.Convert(art.Name, "_").None()
	}
	art.Renew = time.Now()
	if err := art.Insert(modules.DB); err != nil {
		_, _ = ctx.JSON(NewErr("表单数据错误"))
		return
	}
	_, _ = ctx.JSON(NewOK("添加成功"))
}

func ArticleList(ctx iris.Context) {
	order := ctx.PostValueDefault("order", "asc")
	switch order {
	case "asc":
	case "desc":
	default:
		order = "asc"
	}
	arts, err := modules.GetArticleAll(modules.DB)
	if err != nil {
		rst := NewJErr(1, "数据库错误")
		_, _ = ctx.JSON(rst)
		ctx.StopExecution()
	}
	rst := NewJRstOk(arts)
	if _, err := ctx.JSON(rst); err != nil {
		ctx.Application().Logger().Println(err)
	}
}

func ArticleModify(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewJErr(1, "缺少参数"))
		return
	}
	art := new(modules.Article)
	art.Name = dict.Convert(art.Name, "_").None()
	if err := ctx.ReadForm(art); err != nil {
		rst := NewErr(err.Error())
		_, _ = ctx.JSON(rst)
		ctx.StopExecution()
		return
	}
	art.ID = id
	err = art.Update(modules.DB)
	if err != nil {
		rst := NewErr(err.Error())
		_, _ = ctx.JSON(rst)
		return
	}
	_, _ = ctx.JSON(NewOK("修改成功"))
}

func ArticleDel(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewErr(err.Error()))
		return
	}
	err = modules.DelArticleById(modules.DB, id)
	if err != nil {
		_, _ = ctx.JSON(NewErr("数据库错误"))
		return
	}
	_, _ = ctx.JSON(NewOK("删除成功"))
}
