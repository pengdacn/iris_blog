package api

import (
	"github.com/Lofanmi/pinyin-golang/pinyin"
	"github.com/kataras/iris/v12"
	"github.com/pkg/errors"
	"io/ioutil"
	"reflect"
)

var (
	dict = pinyin.NewDict()
)

type (
	conHook       func(ctx iris.Context)
	checkFunc     func() error
	handleErrFunc func(ctx iris.Context, err error)
	_context      struct {
		preHook   []conHook
		postHook  []conHook
		context   iris.Context
		handleErr handleErrFunc
	}
)

func newCon(ctx iris.Context) *_context {
	return &_context{
		context:  ctx,
		preHook:  make([]conHook, 0),
		postHook: make([]conHook, 0),
	}
}

func defaultCon(ctx iris.Context) *_context {
	_ctx := newCon(ctx)
	_ctx.preAppend(addViewData)
	return _ctx
}

func (c *_context) logPrint(item ...interface{}) {
	c.context.Application().Logger().Print(item...)
}

func (c *_context) logPrintln(item ...interface{}) {
	c.context.Application().Logger().Println(item...)
}

func (c *_context) logPrintf(format string, item ...interface{}) {
	c.context.Application().Logger().Printf(format, item...)
}

func (c *_context) preAppend(preHook ...conHook) {
	c.preHook = append(c.preHook, preHook...)
}

func (c *_context) postAppend(postHook ...conHook) {
	c.preHook = append(c.postHook, postHook...)
}

func (c *_context) addViewData(p iris.Map) {
	for k, v := range p {
		c.context.ViewData(k, v)
	}
}

func (c *_context) checkErr(check checkFunc, handle handleErrFunc) {
	err := check()
	if err != nil {
		if handle == nil {
			printlnErr(c.context, err)
		} else {
			handle(c.context, err)
		}
	}
}

func (c *_context) executeErr(check checkFunc) {
	c.checkErr(check, c.handleErr)
}

func addViewData(ctx iris.Context) {
	if Cfg.GetMust("Rewrite") == "1" {
		ctx.ViewData("rewrite", "true")
	} else {
		ctx.ViewData("rewrite", "false")
	}
}

func printlnErr(ctx iris.Context, err error) {
	ctx.Application().Logger().Println(err)
}

func authU(u, pass string) bool {
	if u == Cfg.GetMust("Author") && pass == Cfg.GetMust("Password") {
		return true
	}
	return false
}

func GetThemeDir() []string {
	fd, err := ioutil.ReadDir("./views")
	if err != nil {
		panic(errors.WithStack(err))
	}
	rst := make([]string, 0)
	for _, f := range fd {
		if f.IsDir() && f.Name() != "admin" {
			rst = append(rst, f.Name())
		}
	}
	return rst
}

type jsonData struct {
	Code  int         `json:"code"`
	Msg   string      `json:"msg"`
	Count int         `json:"count"`
	Data  interface{} `json:"data"`
}

func NewJOk(msg ...string) *jsonData {
	rst := new(jsonData)
	rst.Code = 0
	if msg != nil {
		rst.Msg = msg[0]
	} else {
		rst.Msg = "OK"
	}
	return rst
}

func NewJErr(code int, msg ...string) *jsonData {
	rst := new(jsonData)
	rst.Code = code
	if msg != nil {
		rst.Msg = msg[0]
	} else {
		rst.Msg = "Error"
	}
	return rst
}

func NewJRstOk(data interface{}) *jsonData {
	rst := NewJOk()
	if data == nil {
		return rst
	}
	rst.Data = data
	if reflect.TypeOf(data).Kind() == reflect.Slice {
		val := reflect.ValueOf(data)
		rst.Count = val.Len()
	} else {
		rst.Count = 1
	}
	return rst
}

type resultData struct {
	Error int
	Title string
	Msg   string
	Data  interface{}
}

func NewOK(msg ...string) *resultData {
	rst := new(resultData)
	rst.Title = "成功"
	rst.Error = 0
	if msg != nil && len(msg) > 0 {
		rst.Msg = msg[0]
	}
	return rst
}

func NewErr(msg ...string) *resultData {
	rst := NewOK(msg...)
	rst.Error = 1
	rst.Title = "失败"
	return rst
}

func NewRstOK(data interface{}) *resultData {
	rst := NewOK()
	rst.Data = data
	return rst
}
