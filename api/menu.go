package api

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"iris_blog/modules"
)

func MenuAdd(ctx iris.Context) {
	name := ctx.PostValue("name")
	url := ctx.PostValue("url")
	description := ctx.PostValue("description")
	if name == "" || url == "" || description == "" {
		_, _ = ctx.JSON(NewErr("数据错误"))
		ctx.StopExecution()
		return
	}
	menu := new(modules.Link)
	{
		menu.Name = name
		menu.Url = url
		menu.Description = description
		menu.Type = 1
	}
	err := menu.Insert(modules.DB)
	if err != nil {
		_, _ = ctx.JSON(NewErr("数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("添加成功"))
}

func MenuList(ctx iris.Context) {
	order := ctx.URLParam("order")
	switch order {
	case "asc":
	case "desc":
	default:
		order = "asc"
	}
	menus, err := modules.GetLinkAll(modules.DB.Order(fmt.Sprintf("id %s", order)).Where("type = 1"))
	if err != nil {
		_, _ = ctx.JSON(NewJErr(1, "数据库查询错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewJRstOk(menus))
}

func MenuDel(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewErr("get 数据错误"))
		ctx.StopExecution()
		return
	}
	err = modules.DelLinkByID(modules.DB, id)
	if err != nil {
		_, _ = ctx.JSON(NewErr("数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("删除成功"))
}

func MenuModify(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewErr("get 数据错误"))
		ctx.StopExecution()
		return
	}
	renewMenu := new(modules.Link)
	if ctx.ReadForm(renewMenu) != nil {
		_, _ = ctx.JSON(NewErr("form 数据错误"))
		ctx.StopExecution()
		return
	}
	renewMenu.ID = id
	err = renewMenu.Update(modules.DB)
	if err != nil {
		_, _ = ctx.JSON(NewErr("数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("OK"))
}
