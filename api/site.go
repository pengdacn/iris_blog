package api

import (
	"github.com/kataras/iris/v12"
	"iris_blog/modules"
)

func SiteModify(ctx iris.Context) {
	all := ctx.FormValues()
	updateConfig := make([]modules.Config, 0, len(all))
	for k, v := range all {
		updateConfig = append(updateConfig, modules.Config{Option: k, Value: v[0]})
	}
	for _, k := range updateConfig {
		err := k.Update(modules.DB)
		if err != nil {
			_, _ = ctx.JSON(NewErr(err.Error()))
			ctx.StopExecution()
			return
		}
	}
	initCfg()
	_, _ = ctx.JSON(NewOK("更新成功"))
}
