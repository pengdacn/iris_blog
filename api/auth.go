package api

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/sessions"
	"net/http"
	"time"
)

var sess = sessions.New(sessions.Config{
	Cookie:  "master",
	Expires: 30 * time.Minute,
})

func Login(ctx iris.Context) {
	user := ctx.PostValueDefault("username", "")
	passwd := ctx.PostValueDefault("password", "")
	if user == "" {
		ctx.ViewData("config", Cfg.GetCfg())
		if err := ctx.View("admin/login.html"); err != nil {
			ctx.Application().Logger().Println(err)
		}
		return
	}
	session := sess.Start(ctx)
	if authU(user, passwd) {
		session.Set("Authenticated", user)
		_, _ = ctx.JSON(NewOK("欢迎回来"))
		return
	} else {
		_, _ = ctx.JSON(NewErr("账号或者密码错误"))
	}
}

func Logout(ctx iris.Context) {
	session := sess.Start(ctx)
	session.Delete("Authenticated")
	ctx.Redirect("/login", http.StatusFound)
}

func AuthLogin(ctx iris.Context) {
	session := sess.Start(ctx)
	if str := session.GetString("Authenticated"); str != "" {
		ctx.Values().Set("master", str)
		ctx.Next()
		return
	}
	ctx.Redirect("/login", http.StatusFound)
}
