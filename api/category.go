package api

import (
	"github.com/kataras/iris/v12"
	"iris_blog/modules"
)

func CategoryList(ctx iris.Context) {
	cs, err := modules.GetCategoryAll(modules.DB)
	if err != nil {
		_, _ = ctx.JSON(NewJErr(1, "数据库查询错误"))
		return
	}
	_, _ = ctx.JSON(NewJRstOk(cs))
}

func CategoryAdd(ctx iris.Context) {
	ca := new(modules.Category)
	if err := ctx.ReadForm(ca); err != nil {
		_, _ = ctx.JSON(NewErr("错误的表单数据"))
		ctx.StopExecution()
		return
	}
	if err := ca.InsertIdentified(modules.DB); err != nil {
		_, _ = ctx.JSON(NewErr(err.Error()))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("添加成功"))
}

func CategoryDel(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewErr("错误的get参数"))
		ctx.StopExecution()
		return
	}
	if err := modules.DelCategoryById(modules.DB, id); err != nil {
		_, _ = ctx.JSON(NewErr(err.Error()))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("删除成功"))
}

func CategoryModify(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewErr("错误的get参数"))
		ctx.StopExecution()
		return
	}
	c := new(modules.Category)
	if err := ctx.ReadForm(c); err != nil {
		_, _ = ctx.JSON(NewErr(err.Error()))
		ctx.StopExecution()
		return
	}
	c.ID = id
	if err := c.UpdateByID(modules.DB); err != nil {
		_, _ = ctx.JSON(NewErr(err.Error()))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("修改成功"))
}
