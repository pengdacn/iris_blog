package api

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"iris_blog/modules"
)

func CommentAdd(ctx iris.Context) {
	comm := new(modules.Comment)
	if ctx.ReadForm(comm) != nil {
		_, _ = ctx.JSON(NewErr("form 数据错误"))
		ctx.StopExecution()
		return
	}
	if Cfg.GetMust("Comment") == "on" {
		comm.Status = 0
	} else {
		comm.Status = 1
	}
	if comm.Insert(modules.DB) != nil {
		_, _ = ctx.JSON(NewErr("数据库错误"))
		return
	}
	_, _ = ctx.JSON(NewOK())
}

func CommentDel(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewErr("get 数据错误"))
		ctx.StopExecution()
		return
	}
	if err := modules.DelCommentByID(modules.DB, id); err != nil {
		_, _ = ctx.JSON(NewErr("数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("删除成功"))
}

// 待审核的评论
func CommentReview(ctx iris.Context) {
	order := ctx.URLParam("order")
	switch order {
	case "asc":
	case "desc":
	default:
		order = "asc"
	}
	comments, err := modules.GetCategoryAll(modules.DB.Order(fmt.Sprintf("id %s", order)).Where("type = 1"))
	if err != nil {
		_, _ = ctx.JSON(NewJErr(1, "数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewJRstOk(comments))
}

func CommentAdopt(ctx iris.Context) {
	order := ctx.URLParam("order")
	switch order {
	case "asc":
	case "desc":
	default:
		order = "asc"
	}
	comments, err := modules.GetCategoryAll(modules.DB.Order(fmt.Sprintf("id %s", order)).Where("type = 0"))
	if err != nil {
		_, _ = ctx.JSON(NewJErr(1, "数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewJRstOk(comments))
}
