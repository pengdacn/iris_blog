package api

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"iris_blog/modules"
)

func LinkAdd(ctx iris.Context) {
	l := new(modules.Link)
	if ctx.ReadForm(l) != nil {
		_, _ = ctx.JSON(NewErr("参数错误"))
		ctx.StopExecution()
		return
	}
	l.Type = 0
	err := l.Insert(modules.DB)
	if err != nil {
		_, _ = ctx.JSON(NewErr("数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("创建成功"))
}

func LinkList(ctx iris.Context) {
	order := ctx.URLParam("order")
	switch order {
	case "asc":
	case "desc":
	default:
		order = "asc"
	}
	links, err := modules.GetLinkAll(modules.DB.Order(fmt.Sprintf("id %s", order)).Where("type = 0"))
	if err != nil {
		_, _ = ctx.JSON(NewJErr(1, "数据库查询错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewJRstOk(links))
}

func LinkModify(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewErr("get 数据错误"))
		ctx.StopExecution()
		return
	}
	l := new(modules.Link)
	if ctx.ReadForm(l) != nil {
		_, _ = ctx.JSON(NewErr("参数错误"))
		ctx.StopExecution()
		return
	}
	l.ID = id
	err = l.Update(modules.DB)
	if err != nil {
		_, _ = ctx.JSON(NewErr("数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("OK"))
}

func LinkDel(ctx iris.Context) {
	id, err := ctx.URLParamInt("id")
	if err != nil {
		_, _ = ctx.JSON(NewErr("get 数据错误"))
		ctx.StopExecution()
		return
	}
	err = modules.DelLinkByID(modules.DB, id)
	if err != nil {
		_, _ = ctx.JSON(NewErr("数据库错误"))
		ctx.StopExecution()
		return
	}
	_, _ = ctx.JSON(NewOK("OK"))
}
